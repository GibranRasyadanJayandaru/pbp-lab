from django.urls import path
from .views import index, add_friend

urlpatterns = [
    path('', index, name='index'), # access the result by accessing http://localhost:8000/lab-3
    path('add', add_friend, name='add_friend') # access the result by accessing http://localhost:8000/lab-3/add
]    
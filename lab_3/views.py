from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm

# Create your views here.

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all() 
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)


# view untuk render form dan menyimpannya secara langsung ke dalam database bila valid

@login_required(login_url='/admin/login/')
def add_friend(request):
    context = {}

    # menciptakan object form
    form = FriendForm(request.POST or None)

    # memeriksa apakah data form valid dan metode request adalah POST
    if (form.is_valid() and request.method == 'POST'):
        # menyimpan data form ke model
        form.save() 
        return HttpResponseRedirect('/lab-3') # redirect ke tabel utama, object Friend baru muncul

    context['form'] = form
    return render(request, 'lab3_form.html', context) # render form page
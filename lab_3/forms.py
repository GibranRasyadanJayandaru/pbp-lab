from django import forms
from lab_1.models import Friend

# membuat ModelForm
class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend # model yang digunakan
        fields = "__all__" # semua fields di model Friend harus digunakan

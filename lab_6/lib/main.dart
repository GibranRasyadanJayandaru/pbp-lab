// kode ini terinspirasi dari https://www.youtube.com/watch?v=4c36HNckh8A&t=490s

import 'package:flutter/material.dart'; // import material.dart
import 'home.dart'; // import file home.dart

// membuat main function
void main() => runApp(new MyApp());

// main application widget
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // akan mereturn MaterialApp
    return MaterialApp(
      // set tema dari app
      theme: ThemeData(
        primarySwatch: Colors.green,
        fontFamily: 'Raleway',
      ),
      title: 'Caseworqer',
      home: DaftarPerusahaanPage(), // landing page

    );
  }
}

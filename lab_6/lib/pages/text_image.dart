import 'package:flutter/material.dart';

class TextImage extends StatefulWidget {
  @override
  TextImageState createState() => new TextImageState();
}

class TextImageState extends State<Image> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
        backgroundColor: Colors.pinkAccent,
        title: new Align(
          alignment: Alignment.center,
          child: Text("Caseworqer", style: TextStyle(color: Colors.white),),
        )
      ),
      body: new Container(
        child: new Center(
          child: new Column(
            children: <Widget>[
              new Container(
                padding: EdgeInsets.all(10),
                child: new Center(
                  child: RichText(
                    text: TextSpan(
                      text: "This is the image!!!",
                      style: TextStyle(height: 1.75, fontSize: 25, color: Colors.blue[500],),
                      children: <TextSpan>[
                        TextSpan(text: '\n1: ', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.blue),)
                      ],
                    ),
                  ),
                ),
              ),
              new Image(
                image: AssetImage('assets/image1.jpg')
              )
            ]
          ),
        ),
      )
    );
  }
}
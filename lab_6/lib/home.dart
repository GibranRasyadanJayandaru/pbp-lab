import 'package:flutter/material.dart';

// stateful widget yang diinisiasi main application pada main.dart
class DaftarPerusahaanPage extends StatefulWidget {
  const DaftarPerusahaanPage({Key? key}) : super(key: key);

  @override
  State<DaftarPerusahaanPage> createState() => _DaftarPerusahaanPageState();
}
// Kode dibawah terinspirasi dari: https://api.flutter.dev/flutter/material/BottomNavigationBar-class.html

// private state class yang berjalan pada DaftarPerusahaanPage
class _DaftarPerusahaanPageState extends State<DaftarPerusahaanPage> {
  int _selectedIndex = 2; // pertama kali index terpilih adalah index 2, [Akun Saya] tempat profil perusahaan berada

  // tampilan pada page saat BottomNavigationBar di tap
  static const TextStyle menuStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Beranda \n(Coming soon..)',
      style: menuStyle,
    ),
    Text(
      'Index 1: Lamar\n(Coming soon..)',
      style: menuStyle,
    ),
    Text(
      'Index 2: Akun Saya\n(Coming soon..)',
      style: menuStyle,
    ),
  ];

  // menyesuaikan page dengan NavBar saat ini user berada
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    // scaffold berperan seperti roof dari App
    return Scaffold(

      appBar: AppBar(
        title: const Text(
          'Caseworqer', // judul pada AppBar (bar sebelah atas)
          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 25,
              color: Colors.red),
        ),
        backgroundColor: Colors.black,
      ),

      // Kode dibawah terinspirasi dari: https://www.youtube.com/watch?v=4wS5LdXJgEA
      backgroundColor: Colors.greenAccent, // warna latar (belakang card)

      // membuat perusahaan card
      body: Container(
        padding: EdgeInsets.all(15), // padding agar ada space antara container card dengan dimensi device keseluruhan
        child: Stack(
          fit: StackFit.expand, // stack memenuhi screen
          children: [
            latarBelakang(), // setting Card
            Positioned(
              bottom: 55, // atur koordinat foto pelamar
              left: 25,
              child: fotoPelamar(),
            ),
            Positioned(
              right: 8, // atur koordinat text pelamar
              left: 8,
              bottom: 210,
              child: pelamarText(),
            ),
            Positioned(
              right: 8, // atur koordinat informasi perusahaan
              left: 8,
              top: 30,
              bottom: 8,
              child: informasiPerusahaan(),
            ),
          ]
        )
      ),

        // Set BottomNavigationBar
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Beranda',
            ),
            BottomNavigationBarItem(
            icon: Icon(Icons.add),
            label: 'Lamar',
            ),
            BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Akun Saya',
            ),
           ],
           currentIndex: _selectedIndex,
           selectedItemColor: Colors.green,
           onTap: _onItemTapped,
          ),
      );
  }

  // custom Card
  Widget latarBelakang() => ClipRRect(
    borderRadius: BorderRadius.circular(24), // ujung tumpul
    child: Container(
      color: Colors.amberAccent, // warna latar belakang
    )
  );

  // detail perusahaan
  Widget informasiPerusahaan() => Column(
    crossAxisAlignment: CrossAxisAlignment.center, // membuat text center terhadap sumbu x
    mainAxisSize: MainAxisSize.min, // meminimalkan jumlah free space saat flex layout
    children: [
      Text(
        'PT Adidas Indonesia',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 25,
          color: Colors.black,
        ),
        textAlign: TextAlign.center,
      ),
      const SizedBox(height: 16),
      Container(
        padding: EdgeInsets.symmetric(horizontal: 25), // text keterangan diberi padding agar tidak mentok thdp sisi horizontal container
        child: Text(
          '\nAlamat: Jalan M. T. Haryono No. 45, Jakarta Selatan 12770'
              '\n\n Email: employment@adidas.co.id'
              '\n\n Telepon: 02143255422'
              '\n\n Tentang Perusahaan:'
              '\nAdidas-Salomon AG. atau yang lebih '
              'dikenal dengan Adidas merupakan merupakan perusahaan yang '
              'memproduksi sepatu dan beberapa perlengkapan olahraga lain '
              'yang berpusat di Herzogenaurach, Jerman. Adidas mempunyai '
              'komitmen untuk memproduksi peralatan olahraga untuk beberapa '
              'atlet dalam olahraga "pinggiran", seperti lompat jauh yang atletnya '
              'bernama Dick Fosbury melompat dengan memakai sepatu buatan Adidas.',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 12,
            color: Colors.black,
          ),
          textAlign: TextAlign.justify,
        ),
      ),
    ],
  );

  // text judul pelamar
  Widget pelamarText() => Column(
    crossAxisAlignment: CrossAxisAlignment.center, // membuat text center terhadap sumbu x
    mainAxisSize: MainAxisSize.min,
    children: [
      Text(
        'Pelamar',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 25,
          color: Colors.black,
        ),
        textAlign: TextAlign.center,
      ),
    ],
  );

  // foto profil pelamar
  Widget fotoPelamar() => ClipRRect(
    borderRadius: BorderRadius.circular(8.0),
      child: Image.network(
      'https://www.w3schools.com/howto/img_avatar.png',
      scale: 4
      )
  );

}
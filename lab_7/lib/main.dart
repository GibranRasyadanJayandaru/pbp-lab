// kode ini terinspirasi dari https://www.youtube.com/watch?v=4c36HNckh8A&t=490s

import 'package:flutter/material.dart'; // import material.dart
import 'daftar_perusahaan.dart'; // import file DaftarPerusahaanPage
import 'form_profil_perusahaan.dart'; // import FormProfilPerusahaan

// membuat main function
void main() => runApp(new MyApp());

// main application widget
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static final String title = 'Caseworqer';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: title, // set judul app
      // set tema dari app
      theme: ThemeData(
          primaryColor: Colors.green,
          fontFamily: 'Raleway'
      ),
      home: MainPage(title: title), // landing page
    );
  }
}

// stateful widget yang diinisiasi main application pada main.dart
class MainPage extends StatefulWidget {
  final String title;

  // menerima parameter berupa judul dari page, dalam hal ini, nama app
  const MainPage({
    required this.title,
  });

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  // pertama kali index terpilih adalah index 1, [Profil Perusahaan]
  int index = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.greenAccent, // warna latar belakang default
      appBar: AppBar(
        title: const Text(
          'Caseworqer', // judul pada AppBar (bar sebelah atas)
          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 25,
              color: Colors.red),
        ),
        backgroundColor: Colors.black,
      ),

      // Kode dibawah diadaptasi dari: https://api.flutter.dev/flutter/material/BottomNavigationBar-class.html
      body: buildPages(), // menginisiasi page sesuai dengan current index BottomNavigationBar
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: index,
        backgroundColor: Colors.lightGreen, // background color BottomNavBar
        selectedItemColor: Colors.white70, // warna selected BottomNavBar item

        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.format_align_justify_sharp),
            title: Text('Isi Profil'), // untuk isi profil perusahaan
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_balance),
            title: Text('Profil Perusahaan'), // melihat profil perusahaan yang ada
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Akun Saya'), // Akun saya (belum diimplementasi)
          ),
        ],
        onTap: (int index) => setState(() => this.index = index), // update current index dr BottomNavBar yg terpilih
      ),
    );
  }

  // membangun page yang bersesuaian dengan index BottomNavBar yang terpilih
  Widget buildPages() {
    switch (index) {
      case 0:
        return FormProfilPerusahaan();
      case 1:
        return DaftarPerusahaanPage();
      default:
        return Container(
          child: Center(
            child: Text(
              "Akun Saya Page\n(Modul lain)",
              textAlign: TextAlign.center,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 25,
                  color: Colors.black)
            ),
          ),
        );
    }
  }
}

import 'package:flutter/material.dart';
import 'package:lab_7/main.dart';

// Kode ini terinspirasi dari: https://www.youtube.com/watch?v=C5hJIKCTrvk
class FormProfilPerusahaan extends StatefulWidget {
  @override
  _FormProfilPerusahaanState createState() => _FormProfilPerusahaanState();
}

class _FormProfilPerusahaanState extends State<FormProfilPerusahaan> {
  // menciptakan controller untuk setiap textField, berguna untuk clear input & cetak ke console
  final namaPerusahaanController = TextEditingController();
  final alamatPerusahaanController = TextEditingController();
  final emailPerusahaanController = TextEditingController();
  final teleponPerusahaanController = TextEditingController();
  final deskripsiPerusahaanController = TextEditingController();


  @override
  void initState() {
    super.initState();

    // state awal dari form
    namaPerusahaanController.addListener(() => setState(() {}));
  }


  // membangun dan menata tiap-tiap widget
  @override
  Widget build(BuildContext context) => Center(
    child: ListView(
      padding: EdgeInsets.all(32), // padding agar ada space antara textfield dengan dimensi device keseluruhan
      children: [
        // membangun masing-masing TextField Widget & submit button serta jarak antar widget
        buildNamaPerusahaan(),
        const SizedBox(height: 24),
        buildAlamatPerusahaan(),
        const SizedBox(height: 24),
        buildEmailPerusahaan(),
        const SizedBox(height: 24),
        buildTeleponPerusahaan(),
        const SizedBox(height: 24),
        buildDeskripsiPerusahaan(),
        const SizedBox(height: 24),
        ButtonWidget(
          text: 'Submit',
          onClicked: () {
            // cetak masing-masing informasi ke console dan kembali ke page detail perusahaan
            print('Nama Perusahaan: ${namaPerusahaanController.text}');
            print('Alamat Perusahaan: ${alamatPerusahaanController.text}');
            print('Email Perusahaan: ${emailPerusahaanController.text}');
            print('Telepon Perusahaan: ${teleponPerusahaanController.text}');
            print('Deskripsi Perusahaan: ${deskripsiPerusahaanController.text}');
            // kembali ke page detail perusahaan
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MainPage(title: 'Caseworqer',)),
            );
          },
        ),
      ],
    ),
  );

  // input field untuk Nama perusahaan
  Widget buildNamaPerusahaan() => TextField(
    controller: namaPerusahaanController, // binding controller yang bersesuaian
    decoration: InputDecoration(
      filled: true,
      fillColor: Colors.white, // membuat isi dari textfield berwarna putih
      hintText: 'PT Makmur Adil Sejahtera',
      labelText: 'Nama Perusahaan',
      prefixIcon: Icon(Icons.account_balance),
      suffixIcon: namaPerusahaanController.text.isEmpty
          ? Container(width: 0) // bila field kosong, tidak muncul quick clear icon button
          : IconButton(
        icon: Icon(Icons.close),
        onPressed: () => namaPerusahaanController.clear(), // quick clear isi dari textfield dengan tap close icon
      ),
      border: OutlineInputBorder(), // menciptakan border pada field
    ),
    textInputAction: TextInputAction.done, // tipe dari action button yang digunakan pada keyboard.
  );

  Widget buildAlamatPerusahaan() => TextField(
    controller: alamatPerusahaanController,
    decoration: InputDecoration(
      filled: true,
      fillColor: Colors.white,
      hintText: 'Jl. M.T. Haryono No. 12, RT.3/RW.1 Cikoko, Kec. Pancoran, Jakarta Timur, DKI Jakarta 13630',
      labelText: 'Alamat Perusahaan',
      prefixIcon: Icon(Icons.location_on_outlined),
      suffixIcon: alamatPerusahaanController.text.isEmpty
          ? Container(width: 0)
          : IconButton(
        icon: Icon(Icons.close),
        onPressed: () => alamatPerusahaanController.clear(),
      ),
      border: OutlineInputBorder(),
    ),
    textInputAction: TextInputAction.done,
  );

  Widget buildEmailPerusahaan() => TextField(
    controller: emailPerusahaanController,
    decoration: InputDecoration(
      filled: true,
      fillColor: Colors.white,
      hintText: 'employment@makmursejahtera.com',
      labelText: 'Email Perusahaan',
      prefixIcon: Icon(Icons.mail),
      suffixIcon: emailPerusahaanController.text.isEmpty
          ? Container(width: 0)
          : IconButton(
        icon: Icon(Icons.close),
        onPressed: () => emailPerusahaanController.clear(),
      ),
      border: OutlineInputBorder(),
    ),
    keyboardType: TextInputType.emailAddress, // agar prompt rekomendasi pada keyboard adalah email address
    textInputAction: TextInputAction.done,
  );

  Widget buildTeleponPerusahaan() => TextField(
        controller: teleponPerusahaanController,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          hintText: '021XXXXXXXX',
          labelText: 'Nomor Telepon Perusahaan',
          prefixIcon: Icon(Icons.phone),
          suffixIcon: teleponPerusahaanController.text.isEmpty
              ? Container(width: 0)
              : IconButton(
            icon: Icon(Icons.close),
            onPressed: () => teleponPerusahaanController.clear(),
          ),
          border: OutlineInputBorder(),
        ),
        keyboardType: TextInputType.number, // agar prompt pada keyboard hanya angka
        textInputAction: TextInputAction.done,
  );

  Widget buildDeskripsiPerusahaan() => TextField(
    controller: deskripsiPerusahaanController,
    decoration: InputDecoration(
      filled: true,
      fillColor: Colors.white,
      hintText: 'PT Makmur Adil Sejahtera adalah perusahaan yang bergerak di bidang konsumsi dengan harga yang bersaing.',
      labelText: 'Deskripsi Perusahaan',
      prefixIcon: Icon(Icons.description),
      suffixIcon: deskripsiPerusahaanController.text.isEmpty
          ? Container(width: 0)
          : IconButton(
        icon: Icon(Icons.close),
        onPressed: () => deskripsiPerusahaanController.clear(),
      ),
      border: OutlineInputBorder(),
    ),
    textInputAction: TextInputAction.done,
  );

}

// pembuatan button terinspirasi dari: https://www.youtube.com/watch?v=ytlDKJBxW_A&t=1s
class ButtonWidget extends StatelessWidget {
  final String text;
  final VoidCallback onClicked;

  const ButtonWidget({
    required this.text,
    required this.onClicked,
  }) : super();

  @override
  Widget build(BuildContext context) => RaisedButton(
    child: Text(
      text,
      style: TextStyle(fontSize: 24),
    ),
    shape: StadiumBorder(),
    color: Theme.of(context).primaryColor,
    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
    textColor: Colors.white,
    onPressed: onClicked,
  );
}
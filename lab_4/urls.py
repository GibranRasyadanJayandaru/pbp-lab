from django.urls import path
from .views import index, add_note, note_list

urlpatterns = [
    path('', index, name='index'), # access the result by accessing http://localhost:8000/lab-4.
    path('add-note', add_note, name='add_note'), # access the result by accessing http://localhost:8000/lab-4/add-note.
    path('note-list', note_list, name='note_list') # access the result by accessing http://localhost:8000/lab-4/note-list.
]

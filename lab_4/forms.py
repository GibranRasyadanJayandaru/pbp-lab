from django import forms
from lab_2.models import Note

# membuat ModelForm
class NoteForm(forms.ModelForm):
    class Meta:
        model = Note # model yang digunakan
        fields = "__all__" # semua fields di model Note harus digunakan

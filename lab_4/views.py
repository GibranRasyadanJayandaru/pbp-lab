from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.

@login_required(login_url='/admin/login/')
def index(request):
    notes = Note.objects.all()
    response = {'notes' : notes}
    return render(request, 'lab4_index.html', response)

# view untuk render form dan menyimpannya secara langsung ke dalam database bila valid

@login_required(login_url='/admin/login/')
def add_note(request):
    context = {}

    # menciptakan object form
    form = NoteForm(request.POST or None)

    # memeriksa apakah data form valid dan metode request adalah POST
    if (form.is_valid() and request.method == 'POST'):
        # menyimpan data form ke model
        form.save() 
        return HttpResponseRedirect('/lab-4') # redirect ke tabel utama, object Note baru muncul

    context['form'] = form
    return render(request, 'lab4_form.html', context) # render form page

# representasi Note dalam bentuk Card
@login_required(login_url='/admin/login/')
def note_list(request):
    notes = Note.objects.all()
    response = {'notes' : notes}
    return render(request, 'lab4_note_list.html', response)
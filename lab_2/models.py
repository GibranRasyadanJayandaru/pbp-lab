from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=30)
    dari = models.CharField(max_length=30, verbose_name='From')
    title = models.CharField(max_length=30)
    message = models.TextField(max_length=280)

# nama dari instance model adalah nama penerimanya
def __str__(self):
    return self.to
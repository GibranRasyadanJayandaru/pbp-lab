Pertanyaan:

1. Apakah perbedaan antara JSON dan XML?

Jawab:
- Format JSON dapat di parse dengan fungsi standar JavaScript. Dengan demikian, hasil parse merupakan object JavaScript yang siap untuk digunakan. Berbeda dengan format XML yang harus di parse menggunakan XML parser. XML lebih sulit untuk di parse daripada JSON karena harus menyesuaikan dengan bahasa pemrograman yang akan menggunakan data tersebut. 

- Format JSON mensupport tipe data array untuk menampilkan data, sedangkan XML tidak.

- JSON di extend dari bahasa pemrograman JavaScript, sedangkan XML di extend dari Standard Generalized Markup Language (SGML)

- Data yang direpresentasikan dalam XML bertipe string. Sedangkan pada JSON, tipe data yang direpresentasikan dapat bertipe string, number, array, null, serta boolean.

- JSON lebih banyak dipakai untuk pengiriman data antara server dan browser karena format JSON yang berorientasi pada data (data-oriented), sebaliknya XML lebih banyak dipakai untuk menyimpan informasi pada sisi server karena format XML yang berorientasi pada dokumen (document-oriented).

2. Apakah perbedaan antara HTML dan XML?

Jawab:

- HTML bersifat statis sehingga digunakan untuk menampilkan data, bukan mengirim (transport) data. Sedangkan, walaupun XML bersifat document-oriented, XML bersifat dinamis sehingga umum digunakan untuk mengirim (transport) data dan tidak untuk menampilkan data.

- Tags pada HTML telah didefinisikan sebelumnya (predefined). Sedangkan pada XML, tags didefinisikan oleh user.

- XML ketat (wajib) mengenai penggunaan closing tags, sementara HTML tidak ketat karena adanya void/singleton elements yang tidak membutuhkan closing tags seperti: `<br>`, `<hr>`.

- Pada XML dapat menggunakan whitespace, sedangkan HTML tidak.

- XML mendukung konsep namespaces. Hal tersebut mampu untuk menghindari adanya kesamaan penamaan saat digabungkan dengan dokumen lain. Berbeda dengan HTML yang tidak mendukung konsep namespaces. Kesamaan penamaan dapat dihindari dengan menggunakan prefix pada nama object member atau dengan melakukan nesting pada object tersebut.

Referensi:
[https://www.w3schools.com/js/js_json_xml.asp](https://www.w3schools.com/js/js_json_xml.asp)
[https://www.javatpoint.com/json-vs-xml](https://www.javatpoint.com/json-vs-xml)
[https://www.educba.com/json-vs-xml/](https://www.educba.com/json-vs-xml/)
[https://www.geeksforgeeks.org/html-vs-xml/](https://www.geeksforgeeks.org/html-vs-xml/)
[https://www.guru99.com/xml-vs-html-difference.html](https://www.guru99.com/xml-vs-html-difference.html)

